module.exports = {
  transpileDependencies: [
    'vuetify'
  ],
  publicPath: process.env.NODE_ENV === 'production' ? 'https://temetz.gitlab.io/elaintesti/' : undefined
}
