import Vue from 'vue'
import Vuetify from 'vuetify/lib/framework'

Vue.use(Vuetify)

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#71a832'
      },
      dark: {
        primary: '#578425'
      }
    }
  }
})
