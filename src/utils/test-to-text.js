export default (testname) => {
  const names = {
    'bird-images': 'Linnut - kuvat',
    'bird-sounds': 'Linnut - äänet',
    mammals: 'Nisäkkäät',
    reptiles: 'Matelijat',
    fish: 'Kalat'
  }
  return names[testname]
}
