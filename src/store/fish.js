export default [
  { group: 'helpot', name: 'Ahven', image: ['ahven.png'] },
  { group: 'lonkerot', name: 'Ankerias', image: ['ankerias.png'] },
  { group: 'all', name: 'Harjus', image: ['harjus.png', 'harjus2.jpg'] },
  { group: 'helpot', name: 'Hauki', image: ['hauki.png'] },
  { group: 'pohja', name: 'Kiiski', image: ['kiiski.png', 'kiiski2.jpg'] },
  { group: 'all', name: 'Kuha', image: ['kuha.png', 'kuha2.jpg'] },
  { group: 'littana', name: 'Lahna', image: ['lahna.jpg'] },
  { group: 'helpot', name: 'Lohi', image: ['lohi.png'] },
  { group: 'pohja', name: 'Made', image: ['made.png', 'made2.jpg'] },
  { group: 'sarjet', name: 'Muikku', image: ['muikku.png'] },
  { group: 'lonkerot', name: 'Nahkiainen', image: ['nahkiainen.jpg'] },
  { group: 'sarjet', name: 'Särki', image: ['sarki.png'] },
  { group: 'littana', name: 'Säyne', image: ['sayne.jpg', 'sayne2.jpg'] },
  { group: 'sarjet', name: 'Seipi', image: ['seipi.jpg', 'seipi2.jpg'] },
  { group: 'sarjet', name: 'Siika', image: ['siika.png', 'siika2.jpg'] },
  { group: 'sarjet', name: 'Silakka', image: ['silakka.jpg', 'silakka2.jpg'] },
  { group: 'helpot', name: 'Taimen', image: ['taimen.png'] }
]
