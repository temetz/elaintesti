import Vue from 'vue'
import Vuex from 'vuex'
import BirdSounds from './bird-sounds'
import BirdImages from './bird-images'
import Mammals from './mammals'
import Reptiles from './reptiles'
import Fish from './fish'

Vue.use(Vuex)

const getRandom = (arr, n) => {
  var result = new Array(n)
  var len = arr.length
  var taken = new Array(len)
  if (n > len) { throw new RangeError('getRandom: more elements taken than available') }
  while (n--) {
    var x = Math.floor(Math.random() * len)
    result[n] = arr[x in taken ? taken[x] : x]
    taken[x] = --len in taken ? taken[len] : len
  }
  return result
}

const randomInteger = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min
}

export default new Vuex.Store({
  state: {
    surveytypes: {
      'bird-sounds': BirdSounds,
      'bird-images': BirdImages,
      mammals: Mammals,
      reptiles: Reptiles,
      fish: Fish
    },
    scoring: JSON.parse(localStorage.getItem('scoring')) || {},
    settings: {
      maxSurveyOptions: parseInt(localStorage.getItem('max-survey-options') || 4),
      darkMode: localStorage.getItem('darkmode') === 'true'
    }
  },
  getters: {
    scoring: state => state.scoring,
    maxSurveyOptions: state => state.settings.maxSurveyOptions,
    darkMode: state => state.settings.darkMode,
    maxSurveyOptionsSelections: (state) => {
      const max = Math.max(...Object.values(state.surveytypes).map(t => t.length))
      const arr = Array.from(Array(max + 1).keys())
      arr.splice(0, 1)
      return arr
    },
    answerOptionsByType: (state) => (type) => {
      const count = state.settings.maxSurveyOptions
      const groups = [...new Set(state.surveytypes[type].map(item => item.group))]
      const group = groups[Math.floor(Math.random() * groups.length)]
      const availableOptions = state.surveytypes[type].filter(item => item.group === group)
      const options = getRandom(availableOptions, count <= availableOptions.length ? count : availableOptions.length)
      const correctOptionIndex = randomInteger(0, options.length - 1)
      return options.map((option, index) => {
        if (index === correctOptionIndex) {
          const questionItem = option.image
            ? require(`@/assets/${option.image[Math.floor(Math.random() * option.image.length)]}`)
            : require(`@/assets/${option.audio[Math.floor(Math.random() * option.audio.length)]}`)
          return {
            ...option,
            state: 'INCOMPLETE',
            correct: true,
            questionItem
          }
        } else {
          return { ...option, state: 'INCOMPLETE', correct: false }
        }
      })
    }
  },
  mutations: {
    setMaxSurveyOptions (state, val) {
      state.settings.maxSurveyOptions = val
      localStorage.setItem('max-survey-options', val)
    },

    toggleDarkMode (state, val) {
      state.settings.darkMode = val
      localStorage.setItem('darkmode', val)
      // this.$vuetify.theme.dark = !this.$vuetify.theme.dark
    },

    userAnsweredQuestion (state, payload) {
      const scoresForTest = state.scoring[payload.test] || {}
      const scoreForOption = scoresForTest[payload.option.name] || {
        correct_firstattempt: 0,
        correct_total: 0,
        wrong: 0
      }

      if (payload.option.correct) {
        scoreForOption.correct_total += 1
        if (payload.attempt === 1) {
          scoreForOption.correct_firstattempt += 1
        }
      } else {
        scoreForOption.wrong += 1
      }

      state.scoring = {
        ...state.scoring,
        [payload.test]: {
          ...scoresForTest,
          [payload.option.name]: scoreForOption
        }
      }

      localStorage.setItem('scoring', JSON.stringify(state.scoring))
    }
  }
})
