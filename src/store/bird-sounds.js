export default [
  { group: 'all', name: 'Kiuru', audio: ['kiuru.mp3'] },
  { group: 'all', name: 'Lehtopöllö', audio: ['lehtopollo.mp3'] }
  /* { group: 'all', name: 'Metso', audio: ['metso.mp3'] },
  { group: 'all', name: 'Pajulintu', audio: ['pajulintu.mp3'] },
  { group: 'all', name: 'Peippo', audio: ['peippo.mp3'] },
  { group: 'all', name: 'Sepelkyyhky', audio: ['sepelkyyhky.mp3'] },
  { group: 'all', name: 'Taivaanvuohi', audio: ['taivaanvuohi.mp3'] },
  { group: 'all', name: 'Teeri', audio: ['teeri.mp3'] },
  { group: 'all', name: 'Kuikka', audio: ['kuikka.mp3'] },
  { group: 'all', name: 'Kuovi', audio: ['kuovi.mp3'] },
  { group: 'all', name: 'Kurki', audio: ['kurki.mp3'] },
  { group: 'all', name: 'Laulujoutsen', audio: ['laulujoutsen.mp3'] },
  { group: 'all', name: 'Laulurastas', audio: ['laulurastas.mp3'] },
  { group: 'all', name: 'Lehtokerttu', audio: ['lehtokerttu.mp3'] },
  { group: 'all', name: 'Tiltaltti', audio: ['tiltaltti.mp3'] } */
]
