# Eläintesti

**A quiz app for learning different animals and animal sounds.**  
The app was created to aid practicing to a exam.

Stats are stored to the browsers localstorage.   
It would be fairly easy to implement an API to store stats server side for cross-browser experience.

**The live demo is available on:** [https://temetz.gitlab.io/elaintesti/](https://temetz.gitlab.io/elaintesti/)

### Tech stack:
- vuejs
- vuex; Vue storage system like Redux is for React
- vue-router
- Vuetify; Material Design for Vue
- gitlab-ci; to build and publish pages

### Known issues

The audio wont play automatically on some iOS devices. For other devices the audio will be autoplayed after the first interaction with the site. This is a limitation of the modern browsers.

### Available commands:

- Project setup:
  - `npm install`
- Compiles and hot-reloads for development:
  - `npm run serve`
- Compiles and minifies for production:
  - `npm run build`
- Lints and fixes files
  - `npm run lint`
